#Use with [Stash](http://quickstart.atlassian.com/download/stash/get-started/)

Quit typing "git status" so much!

* Shows current branch

* Shows whether Git state is dirty

* Shows number of unpushed commits

Check out a [screenshot](http://i50.tinypic.com/k3t8qc.jpg).

Thanks to [Henrik Nyh](http://henrik.nyh.se/2008/12/git-dirty-prompt) and [Brett Terpstra](http://brettterpstra.com/my-new-favorite-bash-prompt/) for their fantastic resources.